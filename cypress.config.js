const { defineConfig } = require('cypress');

module.exports = defineConfig({
    e2e: {
        baseUrl: 'http://localhost:8000',
        supportFile: false,
        // overkill for our case, screenshots are enough
        video: false
    }
});
